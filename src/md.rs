//! Requires the `markdown` feature enabled.
//! Uses [`pulldown-cmark`](https://crates.io/crates/pulldown-cmark)
//! with all features enabled.

use crate::error::Result;
use std::fmt;

use pulldown_cmark;

/// Renders text to HTML from Markdown.
/// Requires the `markdown` feature to be enabled.
pub fn markdown(s: &dyn fmt::Display) -> Result<String> {
    let text = &s.to_string();
    let parser = pulldown_cmark::Parser::new_ext(text, pulldown_cmark::Options::all());

    let mut buf = String::new();
    pulldown_cmark::html::push_html(&mut buf, parser);

    Ok(buf)
}

/// Shorthand alias for `markdown`.
pub fn md(s: &dyn fmt::Display) -> Result<String> {
    markdown(s)
}

#[cfg(test)]
mod tests {
    #[test]
    fn markdown() {
        assert_eq!(
            super::markdown(&"Hello world, this is a ~~complicated~~ *very simple* example.")
                .unwrap(),
            "<p>Hello world, this is a <del>complicated</del> <em>very simple</em> example.</p>\n"
        );
    }
}
