//! # Askama Filters
//!
//! A collection of additional template filters for the
//! [Askama templating engine](https://github.com/djc/askama)
//!
//! ## Using
//!
//! This library is intended to be used alongside Askama and is
//! effectively useless without it.
//!
//! Inside any module that is defining an Askama `Template`
//! just `use askama_filters::filters;` and they will be available
//! in the HTML template.
//!
//! If you wish to use this library in addition to your own filters
//! create a module named `filters` and add `use askama_filters::filters::*`
//! to it. Then import that module wherever you are creating `Template`s
//! and both sets of filters should be available.
//!
//! A number of filters in this library return HTML markup.
//! For security reasons these are not automatically considered safe
//! and you most likely will want to chain them with the `safe` filter
//! like so `var|markdown|safe`. Escaping inbetween is also recommeneded
//! such as `var|e|md|safe`.
//!
//! ## Features
//!
//! There are a few optional features that can be enabled:
//!
//! - `markdown`: Markdown parsing using a filter.
//! - `date`: Parse and format date/time strings.

mod error;
pub use crate::error::{Error, Result};

#[cfg(feature = "markdown")]
mod md;

#[cfg(feature = "date")]
mod date;

pub mod filters {
    //! The main collection of filters.
    //! These filters do not require a feature flag.

    use super::error::Result;
    use regex as re;
    use std::fmt;

    #[cfg(feature = "markdown")]
    pub use crate::md::*;

    #[cfg(feature = "date")]
    pub use crate::date::*;

    /// Replaces straight quotes with curly quotes,
    /// and turns `--` and `---` to en- and em-dash
    /// and ... into an ellipsis.
    pub fn smarty(s: &dyn fmt::Display) -> Result<String> {
        Ok(s.to_string()
            // Smart Quotes
            .split_whitespace()
            .map(|w| {
                let mut w = String::from(w);
                if w.starts_with('\'') {
                    w = w.replacen('\'', "&lsquo;", 1);
                } else if w.starts_with('"') {
                    w = w.replacen('"', "&ldquo;", 1);
                }
                if w.ends_with('\'') {
                    w.pop();
                    w.push_str("&rsquo;");
                } else if w.ends_with('"') {
                    w.pop();
                    w.push_str("&rdquo;");
                }
                w
            })
            .collect::<Vec<String>>()
            .join(" ")
            // Em-dash first
            .replace("---", "&mdash;")
            // then En-dash
            .replace("--", "&ndash;")
            // Ellipsis
            .replace("...", "&#8230;"))
    }

    /// Converts text into title case.
    pub fn title(s: &dyn fmt::Display) -> Result<String> {
        // Taken from https://gist.github.com/jpastuszek/2704f3c5a3864b05c48ee688d0fd21d7#gistcomment-2125912
        Ok(s.to_string()
            .split_whitespace()
            .map(|w| w.chars())
            .map(|mut c| {
                c.next()
                    .into_iter()
                    .flat_map(|c| c.to_uppercase())
                    .chain(c.flat_map(|c| c.to_lowercase()))
            })
            .map(|c| c.collect::<String>())
            .collect::<Vec<String>>()
            .join(" "))
    }

    /// Expands links into `a` tags to the link
    /// with the same text.
    pub fn link(s: &dyn fmt::Display) -> Result<String> {
        // Taken from https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
        let r = re::Regex::new(
            r"[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)",
        )?;
        Ok(s.to_string()
            .split_whitespace()
            .map(|w| {
                if r.is_match(w) {
                    format!("<a href=\"{}\">{}</a>", w, w)
                } else {
                    String::from(w)
                }
            })
            .collect::<Vec<String>>()
            .join(" "))
    }

    /// Applies a Regular Expression replacement to the text.
    /// Will throw an error if the supplied regex string is invalid.
    /// See the [`regex` crate docs](https://docs.rs/regex) for more info.
    /// The second argument the regex should be a raw string like `r"\d+"`.
    pub fn regex<S, R>(s: S, regex: R, replace: R) -> Result<String>
    where
        S: fmt::Display,
        R: AsRef<str>,
    {
        let r = re::Regex::new(regex.as_ref())?;
        Ok(String::from(
            r.replace_all(&s.to_string(), replace.as_ref()),
        ))
    }

    /// Replaces a prefix at the beginning of a word with
    /// an expanded form. Useful in combination with other filters
    /// or as a shorthand.
    pub fn prefix<S, E>(s: S, prefix: E, expand_to: E) -> Result<String>
    where
        S: fmt::Display,
        E: AsRef<str>,
    {
        Ok(s.to_string()
            .split_whitespace()
            .map(|w| {
                if w.starts_with(prefix.as_ref()) {
                    w.replace(prefix.as_ref(), expand_to.as_ref())
                } else {
                    String::from(w)
                }
            })
            .collect())
    }

    /// Replaces a postfix at the end of a word
    /// with an expanded form. See `prefix`.
    pub fn postfix<S, E>(s: S, postfix: E, expand_to: E) -> Result<String>
    where
        S: fmt::Display,
        E: AsRef<str>,
    {
        Ok(s.to_string()
            .split_whitespace()
            .map(|w| {
                if w.ends_with(postfix.as_ref()) {
                    w.replace(postfix.as_ref(), expand_to.as_ref())
                } else {
                    String::from(w)
                }
            })
            .collect())
    }

    /// Similar to prefix but instead creates an `a` tag with
    /// the matched word as the text and the expanded as the `href` with
    /// the text appended.
    /// Useful for things like #hashtags.
    pub fn tag<S, E>(s: S, prefix: E, expand_to: E) -> Result<String>
    where
        S: fmt::Display,
        E: AsRef<str>,
    {
        Ok(s.to_string()
            .split_whitespace()
            .map(|w| {
                if w.starts_with(prefix.as_ref()) {
                    format!(
                        "<a href=\"{}\">{}</a>",
                        w.replace(prefix.as_ref(), expand_to.as_ref()),
                        w
                    )
                } else {
                    String::from(w)
                }
            })
            .collect())
    }

    /// Formats a slice or `Vec` into an ordered or unordered list.
    /// The `ordered` param controls if it will be an `ol` or `ul` tag.
    pub fn list<T, L>(list: L, ordered: bool) -> Result<String>
    where
        T: fmt::Display,
        L: AsRef<[T]>,
    {
        let ltag = if ordered { "ol" } else { "ul" };
        Ok(format!(
            "<{}>{}</{}>",
            ltag,
            list.as_ref()
                .iter()
                .map(|e| { format!("<li>{}</li>", e) })
                .collect::<String>(),
            ltag
        ))
    }

}

#[cfg(test)]
mod tests {
    use super::filters;

    #[test]
    fn smarty() {
        assert_eq!(
            filters::smarty(&"This is a \"smart\" 'string' don't... -- --- ").unwrap(),
            "This is a &ldquo;smart&rdquo; &lsquo;string&rsquo; don't&#8230; &ndash; &mdash;"
        );
    }

    #[test]
    fn title() {
        assert_eq!(
            filters::title(&"title CASED sTrInG").unwrap(),
            "Title Cased String"
        );
    }

    #[test]
    fn link() {
        assert_eq!(
            filters::link(&"https://rust-lang.org").unwrap(),
            "<a href=\"https://rust-lang.org\">https://rust-lang.org</a>"
        );
    }

    #[test]
    fn regex() {
        assert_eq!(
            filters::regex(&"Test 12345 String", r"\d+", "numbers").unwrap(),
            "Test numbers String"
        );
    }

    #[test]
    fn prefix() {
        assert_eq!(
            filters::prefix("#world", "#", "hello ").unwrap(),
            "hello world"
        );
    }

    #[test]
    fn postfix() {
        assert_eq!(
            filters::postfix("hello#", "#", " world").unwrap(),
            "hello world"
        );
    }

    #[test]
    fn tag() {
        assert_eq!(
            filters::tag("#rust", "#", "https://rust-lang.org/").unwrap(),
            "<a href=\"https://rust-lang.org/rust\">#rust</a>"
        );
    }

    #[test]
    fn list() {
        assert_eq!(
            filters::list(vec![1, 2, 3], false).unwrap(),
            "<ul><li>1</li><li>2</li><li>3</li></ul>"
        );
    }
}
