//! Requires `date` feature enabled.
//! Uses [`chrono`](https://crates.io/crates/chrono).

use crate::error::Result;
use std::fmt;

use chrono;

/// Parses a date/time using the provided format
/// and reformats it according to the provided format string.
/// See the [Chrono `strftime` docs](https://docs.rs/chrono/0.4.6/chrono/format/strftime/index.html)
/// for formatting instructions.
pub fn date<S, F>(s: S, parse_format: F, format: F) -> Result<String>
where
    S: fmt::Display,
    F: AsRef<str>,
{
    let mut par = chrono::format::Parsed::new();
    chrono::format::parse(
        &mut par,
        &s.to_string(),
        chrono::format::StrftimeItems::new(parse_format.as_ref()),
    )?;
    let dt = par.to_datetime_with_timezone(&chrono::offset::Local::now().timezone())?;
    Ok(dt.format(format.as_ref()).to_string())
}

#[cfg(test)]
mod tests {
    #[test]
    fn date() {
        assert_eq!(
            super::date("Sun Jul 8 00:34:60 2001", "%c", "%Y-%m-%d").unwrap(),
            "2001-07-08"
        );
    }
}
