//! Custom error type handling all the errors the
//! filters can throw.

use askama_shared;
use regex;
use std::fmt;

#[cfg(feature = "date")]
use chrono;

pub use std::error::Error as ErrorTrait;

#[derive(Debug)]
pub enum Error {
    Fmt(fmt::Error),
    RegEx(regex::Error),

    #[cfg(feature = "markdown")]
    Markdown,
    #[cfg(feature = "date")]
    ChronoParse(chrono::ParseError),

    /// This error needs to be non-exhaustive as
    /// variants existence depend on features.
    #[doc(hidden)]
    __Nonexhaustive,
}

impl ErrorTrait for Error {
    // Here for compatibility reasons
    fn description(&self) -> &str {
        match self {
            Error::Fmt(e) => e.description(),
            Error::RegEx(e) => e.description(),
            #[cfg(feature = "markdown")]
            Error::Markdown => "Markdown Error",
            #[cfg(feature = "date")]
            Error::ChronoParse(e) => e.description(),
            _ => "unknown error: __Nonexhaustive",
        }
    }

    fn source(&self) -> Option<&(dyn ErrorTrait + 'static)> {
        match self {
            Error::Fmt(ref e) => Some(e),
            Error::RegEx(ref e) => Some(e),
            #[cfg(feature = "markdown")]
            Error::Markdown => None,
            #[cfg(feature = "date")]
            Error::ChronoParse(ref e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error in filter")
    }
}

impl From<fmt::Error> for Error {
    fn from(f: fmt::Error) -> Self {
        Error::Fmt(f)
    }
}

impl From<regex::Error> for Error {
    fn from(f: regex::Error) -> Self {
        Error::RegEx(f)
    }
}

#[cfg(feature = "date")]
impl From<chrono::ParseError> for Error {
    fn from(f: chrono::ParseError) -> Self {
        Error::ChronoParse(f)
    }
}

impl From<Error> for askama_shared::Error {
    fn from(_f: Error) -> Self {
        askama_shared::Error::__Nonexhaustive
    }
}

pub type Result<T> = std::result::Result<T, Error>;
