# Askama Filters

Additional template filters for the
[Askama templating library](https://github.com/djc/askama).

## Using

This library is intended to be used alongside Askama and is
effectively useless without it.

Inside any module that is defining an Askama `Template`
just `use askama_filters::filters;` and they will be available
in the HTML template.

If you wish to use this library in addition to your own filters
create a module named `filters` and add `use askama_filters::filters::*`
to it. Then import that module wherever you are creating `Template`s
and both sets of filters should be available.

## Building

This is a standard Rust project so just use:

```sh
$ cargo build
```

## Features

- `markdown`: adds the `markdown` and `md` filters which can be used to convert
  plain-text to HTML. It is intended to be used like this:
  ```
  {{ raw|e|md|safe }}
  ```
  with the `e` escaping, and the `safe` marking the resultant HTML as safe.

- `chrono` adds the `date` filter for date and time formatting.

## Contributing / Issues

Please email any issues or patches to `rushteve1` at `rushsteve1.us`.
